$no = 1;
$age = 8;

while $no <= $age do
    puts "no is = #$no"
    $no +=1;
end

$i=0;
$val=5;

begin
    puts "i is #$i"
    $i+=1;
end while $i < 4

$wickets = 0;
$totalWickets = 10;

until $wickets > $totalWickets do
    puts "wickets = #$wickets"
    $wickets +=1;
end

$over = 0;
$overs = 10;

begin
    puts "current over = #$over"
    $over +=1;
end until $over > $overs

for value in 5...8
    puts "value is #{value}"
end

$array = [100,200,300,400,500]
$array.each do |num|
    puts "value : #{num}"
end