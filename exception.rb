begin
    file = open("/unexistant_file")
    if file
       puts "File opened successfully"
    end
 rescue
       file = "rescued"
 end
 puts file

 begin
   puts "before raise"
   raise "Unknown error occured"
   puts "after raise"
rescue
   puts "error rescued"
end

begin
   raise 'A test exception.'
rescue Exception => e
   puts e.message
   puts e.backtrace.inspect
ensure
   puts "Ensuring execution"
end

begin
   puts "No errors raised"
rescue Exception => e
   puts e.message
else
   puts "no errors !"
ensure
   puts "ensuring execution"
end

def promptAndGet(prompt)
   puts "in method before throw"
   print "prompt > #{prompt}"
   res = readline.chomp
   throw :quitRequested if res == "!"
   return res
end

catch :quitRequested do
   puts "in catch block"
   name = promptAndGet("Name2: ")
   age = promptAndGet("Age: ")
   sex = promptAndGet("Sex: ")
   # ..
   # process information
end
promptAndGet("Name:")