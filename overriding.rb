class Parent
    def initialize(value1, value2)
        @width = value1;
        @height = value2;
    end

    def showMessage
        puts "This is parent class method"
    end
end

class Child < Parent
    def showMessage
        puts "this is child class method"
    end

    def showArea
        @area = @width * @height;
        puts "Area = #@area";
    end
end

object = Child.new(10,5);
object.showMessage();
object.showArea();
