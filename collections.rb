array = Array[10,2,3,4,5];

for number in array
    puts "current value is : #{number}";
end

length = array.length;
index = 0;

while index < length
    puts " value at index #{index} : #{array[index]}"
    index += 1;
end

hash = Hash["key1" => 10, "key2" => 20, "key3" => 30, "key4" => 40, "key5" => 50 ]
hash.each { |key,value| puts key,value }

puts "#{hash["key3"]}";

puts "hash length : #{hash.length}"

range = (1..6).to_a

puts "range : #{range}"

puts "rage includes 7 ? : #{range.include?(7)}"

minValue = range.min;
maxValue = range.max;

puts "min value in range : #{minValue}"
puts "max value in range : #{maxValue}"

range.each do |item|
    puts "item value : #{item}"
end

if (range === 2)
    puts "within range";
end

marks = 84;

case marks
when 0...35
    puts "fail"
when 35...60
    puts "average"
when 60...75
    puts "good"
when 75...90
    puts "very good"
when 90..100
    puts "outstanding"
end

collectedArray = Array.new
collectedArray = array.collect;
puts "Collected array : #{collectedArray}"
