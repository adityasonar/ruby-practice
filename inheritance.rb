class Parent
    def initialize(value1,value2)
        @width = value1;
        @height = value2;
    end

    def calculateArea
        @width * @height
    end
end

class Child < Parent
    def showArea
        @area = @width * @height;
        puts "Area is : #@area"
    end
end

object = Child.new(20,30);
object.showArea();

