class Customer
    @@no_of_customers = 0;
    def initialize(id, name, addr)
        @@no_of_customers += 1;
        @cust_id = id;
        @cust_name = name;
        @cust_addr = addr;
    end
    def display_details()
        puts "Customer id is : #{@cust_id}"
        puts "Customer name #@cust_name"
        puts "Customer address #@cust_addr"
    end
    def total_customers()
        puts "Total customers are #@@no_of_customers"
    end
end

cust1 = Customer. new("1", "jonh", "parali");
cust2 = Customer. new("2", "poul", "latur");

cust1.display_details();
cust1.total_customers();
cust2.display_details();
cust2.total_customers();
