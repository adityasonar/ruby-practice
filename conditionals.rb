$age = 8;

case $age
when 0..5
    puts "little kid"
when 5..11
    puts "kid"
when 11..17
    puts "teen"
when 18..100
    puts "adult"
else
    puts "don't know"
end

if $age > 15 and $age < 18
    puts "Grown teen"
else
    puts "just teen"
end

$no = 1;

puts "number" if $no

unless $no > 2
    puts "no is #$no"
else
    puts "no is greater"
end

puts "value is 2" unless $no
